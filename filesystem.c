#include "filesystem.h"

DIR *BLV_openDir(const char *path)
{
#ifdef DEBUG
    fprintf(stderr, "BLV_openDir start\r\n");
#endif

    // assert(path != NULL);
    if (path == NULL)
    {
#ifdef DEBUG
        fprintf(stderr, "BLV_openDir end because path is NULL\r\n");
#endif
        return NULL;
    }
    DIR *directory = opendir(path);
    if (directory == NULL)
        perror("opendir failed");

#ifdef DEBUG
    fprintf(stderr, "BLV_openDir end\r\n");
#endif
    return directory;
}

void BLV_rewindDir(DIR *directory_to_rewind)
{
#ifdef DEBUG
    fprintf(stderr, "BLV_rewindDir start\r\n");
#endif

    // assert(directory_to_rewind != NULL);
    if (directory_to_rewind == NULL)
    {
#ifdef DEBUG
        fprintf(stderr, "BLV_rewindDir end because directory_to_rewind is NULL\r\n");
#endif
        return;
    }

    rewinddir(directory_to_rewind);

#ifdef DEBUG
    fprintf(stderr, "BLV_rewindDir end\r\n");
#endif
}

bool BLV_closeDir(DIR *directory_to_close)
{
#ifdef DEBUG
    fprintf(stderr, "BLV_closeDir start\r\n");
#endif

    // assert(directory_to_close != NULL);
    if (directory_to_close == NULL)
    {
#ifdef DEBUG
        fprintf(stderr, "BLV_closeDir end because directory_to_close is NULL\r\n");
#endif
        return false;
    }

    if (closedir(directory_to_close) < 0)
    {
        perror("closedir failed");
        return false;
    }

#ifdef DEBUG
    fprintf(stderr, "BLV_closeDir end\r\n");
#endif

    return true;
}

bool BLV_createDir(const char *path, const Right_file_access_enum mode)
{

#ifdef DEBUG
    fprintf(stderr, "BLV_createDir start\r\n");
#endif

    // assert(directory_to_close != NULL);
    if (path == NULL)
    {
#ifdef DEBUG
        fprintf(stderr, "BLV_createDir end because path is NULL\r\n");
#endif
        return false;
    }

    if (mkdir(path, mode) < 0)
    {
        perror("mkdir failed");
        return false;
    }

#ifdef DEBUG
    fprintf(stderr, "BLV_createDir end\r\n");
#endif
    return true;
}

#ifdef __unix__
bool BLV_removeDir(const char *path, const bool force_remove)
{
#ifdef DEBUG
    fprintf(stderr, "BLV_removeDir start\r\n");
#endif

    if (path == NULL)
    {
#ifdef DEBUG
        fprintf(stderr, "BLV_removeDir end because path is NULL\r\n");
#endif
        return false;
    }

    int ret_rmdir = rmdir(path);
    if (ret_rmdir < 0 && force_remove == false)
    {
        perror("rmdir failed");
        return false;
    }

    // TODO : implementation du parcours du dossier pour vider tous les fichiers
    if (ret_rmdir < 0 && errno == ENOTEMPTY)
    {
    }

#ifdef DEBUG
    fprintf(stderr, "BLV_removeDir end\r\n");
#endif

    return true;
}
#endif

bool BLV_openFile(const char *path, const char *mode, FILE **f)
{
#ifdef DEBUG
    fprintf(stderr, "BLV_openFile start\r\n");
#endif

    if (path == NULL || mode == NULL)
    {
#ifdef DEBUG
        fprintf(stderr, "BLV_openFile end because path is NULL or mode is NULL or twice\r\n");
#endif
        return false;
    }

    *f = fopen(path, mode);
    if (*f == NULL)
    {
        perror("fopen failed");
        return false;
    }

#ifdef DEBUG
    fprintf(stderr, "BLV_openFile end\r\n");
#endif
    return true;
}

bool BLV_closeFile(FILE **f)
{
#ifdef DEBUG
    fprintf(stderr, "BLV_closeFile start\r\n");
#endif

    if (*f == NULL)
    {
#ifdef DEBUG
        fprintf(stderr, "BLV_closeFile end because file is null\r\n");
#endif
        return false;
    }

    if (fclose(*f) == EOF)
    {
        perror("fclose failed");
        return false;
    }

    f = NULL;

#ifdef DEBUG
    fprintf(stderr, "BLV_closeFile end\r\n");
#endif
    return true;
}

bool BLV_removeFile(const char *path)
{
#ifdef DEBUG
    fprintf(stderr, "BLV_removeFile start\r\n");
#endif

    if (path == NULL)
    {
#ifdef DEBUG
        fprintf(stderr, "BLV_removeFile end because path is NULL\r\n");
#endif
        return false;
    }

    if (remove(path) < 0)
    {
        perror("remove failed");
        return false;
    }

#ifdef DEBUG
    fprintf(stderr, "BLV_removeFile end\r\n");
#endif

    return true;
}

bool BLV_readFile(FILE **f, uint8_t *reading_value, uint16_t size_array)
{
#ifdef DEBUG
    fprintf(stderr, "BLV_readFile start\r\n");
#endif

    if (f == NULL || reading_value == NULL)
    {
#ifdef DEBUG
        fprintf(stderr, "BLV_readFile end because f or reading_value is NULL\r\n");
#endif
        return false;
    }

    if (size_array == 1)
    {
#ifdef DEBUG
        fprintf(stderr, "BLV_readFile end because size_array <= 1\r\n");
#endif
        return false;
    }

    char *ret = fgets((char*)reading_value, size_array - 1, *f);
    if (ret == NULL)
    {
        perror("fgets failed");
        return false;
    }

#ifdef DEBUG
    fprintf(stderr, "BLV_readFile end\r\n");
#endif
    return true;
}