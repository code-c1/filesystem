CC=gcc
CFLAGS=-Wall -Wextra -pedantic -lrt -pthread

SRCS=$(wildcard *.c)
OBJS=$(SRCS:.c=.o)
EXEC=test_client

all: $(EXEC)

$(EXEC): $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

doc:
	doxygen $(DOXYGEN_CONFIG_FILE)

clean:
	rm -f $(OBJS) $(EXEC)
