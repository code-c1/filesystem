#include "unit_test.h"


/**
 * \brief main function of program
 *
 * \return 0 if success, negative value instead
 */
int main(void)
{
    do_unit_test();
    return 0;
}