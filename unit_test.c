#include "unit_test.h"

void do_unit_test(void)
{
    // test chemin null
    DIR *test_dir1 = BLV_openDir(NULL);
    if (test_dir1 != NULL)
        fprintf(stderr, "Test directory name is NULL when openning a directory : Failed\r\n");
    else
        fprintf(stderr, "Test directory name is NULL when openning a directory : Success\r\n");

    // test nom directory faux
    DIR *test_dir2 = BLV_openDir("./failed");
    if (test_dir2 != NULL)
        fprintf(stderr, "Test directory name is wrong when openning a directory : Failed\r\n");
    else
        fprintf(stderr, "Test directory name is wrong when openning a directory : Success\r\n");

    // test nom directory bon mais pas au bon endroit
    DIR *test_dir3 = BLV_openDir("./sous_folder");
    if (test_dir3 != NULL)
        fprintf(stderr, "Test directory name is good but wrong pathname when openning a directory : Failed\r\n");
    else
        fprintf(stderr, "Test directory name is good but wrong pathname when openning a directory : Success\r\n");

    // test pas un directory
    DIR *test_dir4 = BLV_openDir("./Test_dir/fichierT");
    if (test_dir4 != NULL)
        fprintf(stderr, "Test directory not directory when openning a directory : Failed\r\n");
    else
        fprintf(stderr, "Test directory not directory when openning a directory : Success\r\n");

    // test nom directory bon et existant
    DIR *test_dir5 = BLV_openDir("./Test_dir");
    if (test_dir5 == NULL)
        fprintf(stderr, "Test directory name is good and exist when openning a directory : Failed\r\n");
    else
        fprintf(stderr, "Test directory name is good and exist when openning a directory : Success\r\n");

    // test nom directory bon et existant sous directory
    DIR *test_dir6 = BLV_openDir("./Test_dir/sous_folder");
    if (test_dir6 == NULL)
        fprintf(stderr, "Test directory name is good and sub-directory when openning a directory : Failed\r\n");
    else
        fprintf(stderr, "Test directory name is good and sub-directory when openning a directory : Success\r\n");

    // test fermer directory a NULL
    DIR *test_dir7 = NULL;
    if (BLV_closeDir(test_dir7) == true)
        fprintf(stderr, "Test close directory when directory is NULL : Failed\r\n");
    else
        fprintf(stderr, "Test close directory when directory is NULL : Success\r\n");

    // test fermer un directory correctement ouvert
    if (BLV_closeDir(test_dir5) == false)
        fprintf(stderr, "Test close directory when directory is good : Failed\r\n");
    else
        fprintf(stderr, "Test close directory when directory is good : Success\r\n");

    BLV_closeDir(test_dir6);

    // test creation d'un repertoire avec un path NULL
    if (BLV_createDir(NULL, EXEC_READ_WRITE_USR | EXEC_READ_WRITE_OUSR | EXEC_READ_WRITE_GRP) == false)
        fprintf(stderr, "Test creat directory when path is NULL : Success\r\n");
    else
        fprintf(stderr, "Test creat directory when path is NULL : Failed\r\n");

    // test creation d'un repertoire avec permission
    if (BLV_createDir("./directoryTest/", EXEC_READ_WRITE_USR | EXEC_READ_WRITE_OUSR | EXEC_READ_WRITE_GRP) == true)
        fprintf(stderr, "Test creat directory when path is good and mode ok : Success\r\n");
    else
        fprintf(stderr, "Test creat directory when path is good and mode ok : Failed\r\n");

    // test remove dir ou path NULL
    if (BLV_removeDir(NULL, false) == false)
        fprintf(stderr, "Test remove directory with NULL path and no force remove : Success\r\n");
    else
        fprintf(stderr, "Test remove directory with NULL path and no force remove : Success\r\n");

    // test remove dir non vide sans forcer
    if (BLV_removeDir("./Test_dir/", false) == false)
        fprintf(stderr, "Test remove directory not empty and no force remove : Success\r\n");
    else
        fprintf(stderr, "Test remove directory not empty and no force remove : Success\r\n");

    // test remove dir vide sans forcer
    if (BLV_removeDir("./directoryTest/", false) == true)
        fprintf(stderr, "Test remove directory when empty and no force remove : Success\r\n");
    else
        fprintf(stderr, "Test remove directory when empty and no force remove : Failed\r\n");

    // test remove file avec path NULL
    if (BLV_removeFile(NULL) == false)
        fprintf(stderr, "Test remove file when path is null : Success\r\n");
    else
        fprintf(stderr, "Test remove file when path is null : Failed\r\n");

    // test remove file avec fichier inconnus
    if (BLV_removeFile("./file.txt") == false)
        fprintf(stderr, "Test remove file when file is unknow : Success\r\n");
    else
        fprintf(stderr, "Test remove file when file is unknow : Failed\r\n");

    // test remove file avec fichier connus
    if (BLV_removeFile("./Test_dir/fichierT") == true)
        fprintf(stderr, "Test remove file when file is known : Success\r\n");
    else
        fprintf(stderr, "Test remove file when file is known : Failed\r\n");

    FILE *fichier = NULL;
    // test ouverture fichier en lecture seul mais inexistant
    if (BLV_openFile("./Test_dir/fichierT", "r", &fichier) == false)
        fprintf(stderr, "Test open file read only unknown : Success\r\n");
    else
        fprintf(stderr, "Test open file read only unknown : Failed\r\n");

    FILE *fichier2 = NULL;
    // test ouverture fichier en ecriture seul
    if (BLV_openFile("./Test_dir/fichierT", "w", &fichier2) == true)
        fprintf(stderr, "Test open file write only : Success\r\n");
    else
        fprintf(stderr, "Test open file write only : Failed\r\n");

    // test fermeture fichier valant NULL
    if (BLV_closeFile(&fichier) == false)
        fprintf(stderr, "Test close file read only valant NULL : Success\r\n");
    else
        fprintf(stderr, "Test close file read only valant NULL : Failed\r\n");

    // test fermeture fichier ouvert en ecriture seul
    if (BLV_closeFile(&fichier2) == true)
        fprintf(stderr, "Test close file write only: Success\r\n");
    else
        fprintf(stderr, "Test close file write only : Failed\r\n");

    FILE *fichier3 = NULL;
    // test lecture d'une donne dans un fichier
    if (BLV_openFile("./Test_dir/read_test.txt", "r", &fichier3) == false)
        fprintf(stderr, "Test read data in file : failed cause open file failed");

    uint8_t value_read[255];
    memset(value_read, 0, 255);
    if (BLV_readFile(&fichier3, value_read, 255) == true)
        fprintf(stderr, "Test read data in file : Success\r\n");
    else
        fprintf(stderr, "Test read data in file  : Failed\r\n");

    printf("Valeur lu : %s\r\n", value_read);
    if (fichier3 != NULL)
        BLV_closeFile(&fichier3);
}