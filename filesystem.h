#ifndef __FILESYSTEM_H__
#define __FILESYSTEM_H__

#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include <errno.h>
#include <assert.h>

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <stdbool.h>

#include <stdint.h>

#ifdef __unix__
#include <unistd.h>
#endif

/**
 * \brief use to know if we print deubg or not
 */
// #define DEBUG 0

/**
 * \brief use to rename all constante for file access to more easy readable value
 */
typedef enum
{
    READ_ONLY_USR = S_IRUSR,
    WRITE_ONLY_USR = S_IWUSR,
    EXEC_ONLY_USR = S_IXUSR,
    EXEC_READ_WRITE_USR = S_IRWXU,

    READ_ONLY_GRP = S_IRGRP,
    WRITE_ONLY_GRP = S_IWGRP,
    EXEC_ONLY_GRP = S_IXGRP,
    EXEC_READ_WRITE_GRP = S_IRWXG,

    READ_ONLY_OUSR = S_IROTH,
    WRITE_ONLY_OUSR = S_IWOTH,
    EXEC_ONLY_OUSR = S_IXOTH,
    EXEC_READ_WRITE_OUSR = S_IRWXO,

    // PREVENT_ERASE_FILE = S_ISVTX,
} Right_file_access_enum;

/**
 * \brief Wrapper of opendir function use to open a directory
 *
 * \param path const path of directory to open
 *
 * \return pointer to DIR open or NULL instead, if path is NULL then return NULL
 */
DIR *BLV_openDir(const char *path);

/**
 * \brief Wrapper of rewinddir function use to rewind a previously open directory
 *
 * \param directory_to_rewind previously open directory who need to be rewind
 */
void BLV_rewindDir(DIR *directory_to_rewind);

/**
 * \brief Wrapper of closedir function use to close a previously open directory
 *
 * \param directory_to_close DIR open and need to be close
 *
 * \return true if success, false instead or if directory_to_close is NULL
 */
bool BLV_closeDir(DIR *directory_to_close);

/**
 * \brief Wrapper of mkdir function use to creat a directory
 *
 * \param path path of directory to creat, contain name of directory
 * \param mode mode represent permission of created directory
 *
 * \return true if success, false instead or if path is NULL
 */
bool BLV_createDir(const char *path, const Right_file_access_enum mode);

#ifdef __unix__
/**
 * \brief Wrapper of rmdir function, use to remove an empty directory
 *
 * \param path path of directory
 * \param force_remove use to know if we have a no empty directory we clean all file in directory and remove it true if remove, false instead
 *
 * \return true if we correctly remove directory, false instead or if path is NULL
 */
bool BLV_removeDir(const char *path, const bool force_remove);
#endif

/**
 * \brief Wrapper of fopen function, use to create/open a file
 *
 * \param path path of file
 * \param mode open mode for file
 * \param f FILE who will be filled in this function with pointer to openned file
 *
 * \return true if success, false instead or if path or mode is NULL
 */
bool BLV_openFile(const char *path, const char *mode, FILE **f);

/**
 * \brief Wrapper of fclose function, use to close a file previously open
 * 
 * \param f file openned
 * 
 * \return true if success, false if f is NULL or an error occur
*/
bool BLV_closeFile(FILE **f);

/**
 * \brief Wrapper of remove function, use to remove a file
 *
 * \param path path of file
 *
 * \return true if success, false if error occur or path is NULL
 */
bool BLV_removeFile(const char *path);

/**
 * \brief Wrapper of read function, use to read a data and store it in reading_value
 * 
 * \param f file openned
 * \param reading_value array of octet read after this function
 * \param size of reading value array
 * 
 * \return true if success, false if f or reading value are NULL or an error occur
*/
bool BLV_readFile(FILE **f, uint8_t *reading_value, uint16_t size_array);

#endif